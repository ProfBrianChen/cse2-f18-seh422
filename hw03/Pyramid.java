/// This code was created on 9/17/18 by Sean Hong for hw03 of CSE 002
/// The purpose of this program is to find the volume inside a square pyramid
//
import java.util.Scanner; // Import the scanner

public class Pyramid {
  public static void main(String args[]) {
    Scanner myScanner = new Scanner(System.in); // Create an instance called myScanner
    
    // Ask for the length of the square pyramid's base
    System.out.print("The square side of the pyramid is (input length): ");
    double checkLength = myScanner.nextDouble();
    
    // Ask for the height of the square pyramid
    System.out.print("The height of the pyramid is (input height): ");
    double checkHeight = myScanner.nextDouble();
    
    // Calculate the volume and print result into the terminal window
    double pyramidVolume = (checkHeight * Math.pow(checkLength, 2.0)) / 3.0;
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);
  }
}