/// This code was created on 9/17/18 by Sean Hong for hw03 of CSE 002
/// The purpose of this program is to find the number of acres of land affected by hurricane precipitation
//
import java.util.Scanner; // Import the scanner

public class Convert {
  public static void main(String args[]) {
    Scanner myScanner = new Scanner(System.in); // Create an instance called myScanner
    
    // Declare constants for unit conversions
    final int acresToSquareFeet = 43560; // 1 acre = 43560 square feet
    final int inchesToFeet = 12; // 1 foot = 12 inches
    final int milesToFeet = 5280; // 1 mile = 5280 feet
    
    // Ask for the amount of land affected in acres
    System.out.print("Enter the affected area in acres: ");
    double checkAcres = myScanner.nextDouble();
    
    // Ask for the amount of rainfall in the affected area
    System.out.print("Enter the rainfall in the affected area in inches: ");
    double checkRainfall = myScanner.nextDouble();
    
    // Calculate and print result into the terminal window
    double amountOfWater = ((checkAcres * acresToSquareFeet) * (checkRainfall / inchesToFeet)) / Math.pow(milesToFeet, 3.0);
    System.out.println(amountOfWater + " cubic miles");
  }
}