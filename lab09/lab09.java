// Sean Hong, 11/28/18, lab09 for CSE 002
// The purpose of this program is to see the effects of passing arrays through helper methods

import java.util.*; // Import all the classes

public class lab09 {
  public static void main(String[] args) {
		// Create arrays to be manipulated
		int[] array0 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);
		int[] array3;
		
		// Utilize helper methods to see its effects on passing the aforementioned arrays to them
		inverter(array0);
		print(array0);
		
		inverter2(array1);
		print(array1);
		
		array3 = inverter2(array2);
		print(array3);
	}
	// Copy array from argument and create a new array that has same values
	public static int[] copy(int[] list) {
		int[] newArray = new int[list.length];
		for (int i = 0; i < list.length; i++) {
			newArray[i] = list[i];
		}
		return newArray;
	}
	// Invert array input and print it out by swapping values with each opposite farthest member subsequently
	public static void inverter(int[] list) {
		for (int i = 0; i < list.length / 2; i++) {
			int temp = list[i]; // Store element temporarily
			list[i] = list[(list.length - 1) - i]; // Get opposite farthest element
			list[(list.length - 1) - i] = temp; // Swap them
		}
	}
	// Invert array using copy of the argument
	public static int[] inverter2(int[] list) {
		int[] newArray = copy(list);
		for (int i = 0; i < newArray.length / 2; i++) {
			int temp = newArray[i]; // Store element temporarily
			newArray[i] = newArray[(newArray.length - 1) - i]; // Get opposite farthest element
			newArray[(newArray.length - 1) - i] = temp; // Swap them
		}
		return newArray;
	}
	// Print arrays out
	public static void print(int[] list) {
		System.out.println(Arrays.toString(list));
	}
}