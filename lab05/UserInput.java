// Sean Hong, 10/10/18, Lab 05 for CSE 002
// The purpose of this program is to display a particular schedule of a student's input
//

import java.util.*; // Import all the classes

public class UserInput {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create the scanner instance
		
		// Create variables for certain aspects of a class
		int courseNumber = 0;
		String departmentName = null;
		int numberOfMeetings = 0;
		String timeOfMeeting = null;
		String instructorName = null;
		int numberOfStudents = 0;
		
		// Loop through and check whether or not their input is valid in comparison to the variable type
		while (true) {
			System.out.print("Enter a course number: ");
			while (courseNumber == 0) {
				if (myScanner.hasNextInt()) {
					courseNumber = myScanner.nextInt();
					break;
				} else {
					System.out.print("Please enter a valid course number: ");
					myScanner.next();
				}
			}
			myScanner.nextLine();
			System.out.print("Enter the name of the department: ");
			while (departmentName == null) {
				if (myScanner.hasNext()) {
					departmentName = myScanner.next();
					break;
				} else {
					System.out.print("Please enter a valid department name: ");
					myScanner.next();
				}
			}
			myScanner.nextLine();
			System.out.print("Enter the number of class meeting times per week: ");
			while (numberOfMeetings == 0) {
				if (myScanner.hasNextInt()) {
					numberOfMeetings = myScanner.nextInt();
					break;
				} else {
					System.out.print("Please enter a valid number of class meeting times per week: ");
					myScanner.next();
				}
			}
			myScanner.nextLine();
			System.out.print("Enter the time the class starts: ");
			while (timeOfMeeting == null) {
				if (myScanner.hasNext()) {
					timeOfMeeting = myScanner.next();
					break;
				} else {
					System.out.print("Please enter a valid time: ");
					myScanner.next();
				}
			}
			myScanner.nextLine();
			System.out.print("Enter the name of the professor: ");
			while (instructorName == null) {
				if (myScanner.hasNext()) {
					instructorName = myScanner.next();
					break;
				} else {
					System.out.print("Please enter a valid name of the professor: ");
					myScanner.next();
				}
			}
			myScanner.nextLine();
			System.out.print("Enter the amount of students in that class: ");
			while (numberOfStudents == 0) {
				if (myScanner.hasNextInt()) {
					numberOfStudents = myScanner.nextInt();
					break;
				} else {
					System.out.print("Please enter a valid amount of students in that class: ");
					myScanner.next();
				}
			}
			break;
		}
		// Print out collective result into the terminal window
		System.out.println("This person takes " + courseNumber + " in the " + departmentName + " department. They meet " + numberOfMeetings + " times per week at " + timeOfMeeting + ". The class has " + numberOfStudents + " students and the professor's name is " + instructorName + ".");
	}
}