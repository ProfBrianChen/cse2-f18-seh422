////// Sean Hong, 9/6/18, Lab 02 for CSE 002
// The purpose of this program is to measure the distance a bike travels.
//
public class Cyclometer {
    // Main method required for every Java program.
   	public static void main(String[] args) {
      // Input data.
      double secsTrip1 = 480;      // Value of how long it took for the first trip in seconds.
      double secsTrip2 = 3220;     // Value of how long it took for the second trip in seconds.
	  	int countsTrip1 = 1561;      // Value of the counts of the first trip.
	  	int countsTrip2 = 9037;      // Value of the counts of the second trip.
      
      double wheelDiameter = 27.0;                              // Value of the diameter of the wheel on the bike.
      double PI = 3.14159;                                     // Value of a mathematical constant pi.
      int feetPerMile = 5280;                                 // Value of how many feets are in a mile.
      int inchesPerFoot = 12;                                // Value of how many inches are in a foot.
      int secondsPerMinute = 60;                            // Value of how many seconds are in a minute.
      double distanceTrip1, distanceTrip2, totalDistance;  // Declare variables for the distances of the trips, as well as total distance.
      
      // Prints the time and counts of the trips.
      System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
      System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");

      distanceTrip1 = countsTrip1 * wheelDiameter * PI;                                 // Gives the distance of trip 1 in inches.
      distanceTrip1 /= inchesPerFoot * feetPerMile;                                    // Gives the distance of trip 1 in miles.
    	distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // Combines the operations used to calculate the distance of trip 1 for trip 2.
	    totalDistance = distanceTrip1 + distanceTrip2;                                 // Record the total distance of both trips.
      
      //Print out the output data.
      System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
	    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
	    System.out.println("The total distance was " + totalDistance + " miles.");

	}  // End of main method.
} // End of class.