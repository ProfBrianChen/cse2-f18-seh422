// Sean Hong, 10/17/18, Lab 06 for CSE 002
// The purpose of this program is to display a particular pattern into the output
//

import java.util.*; // Import all the classes

public class PatternA {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create the scanner instance
		
		// Establish variables for the length of the pyramid and whether or not the loop starts
		int pyramidLength = 0;
		boolean startLoop = true;
		
		while (startLoop) { // Start the loop
			System.out.print("Enter the length of the pyramid as an integer between 1 and 10: "); // Ask for input
			// Check if input is valid
			while (myScanner.hasNext()) {
				if (myScanner.hasNextInt()) {
					pyramidLength = myScanner.nextInt();
					if (pyramidLength >= 1 && pyramidLength <= 10) {
						break;
					} else {
						System.out.print("Please enter the length of the pyramid as an integer between 1 and 10: ");
						myScanner.nextLine();
					}
				} else {
					System.out.print("Please enter a valid pyramid length between 1 and 10: ");
					myScanner.nextLine();
				}
			}
			// Create pyramid pattern
			for (int i = 1; i <= pyramidLength; i++) {
				for (int j = 1; j <= i; j++) {
					System.out.print(j + " ");
				}
				System.out.println();
			}
			startLoop = false; // End loop
		}
	}
}