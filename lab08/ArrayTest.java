// Sean Hong, 11/15/18, lab08 for CSE 002
// The purpose of this program is to experiment with arrays and to print the occurences of certain elements within them

import java.util.*; // Import all the classes

public class ArrayTest {
  public static void main(String[] args) {
		int[] myIntArray = new int[100]; // Create a new array with size 100
		int[] myIntOccurencesArray = new int[100]; // Create a new array with size 100 to hold occurences of numbers
		
		int numberOfOccurences = 0; // Counter for occurences of numbers
		
		// Assign random values to each index
		for (int i = 0; i < myIntArray.length; i++) {
			int j = (int) (Math.random() * (myIntArray.length - 1)); // Randomize values
			myIntArray[i] = j; // Set randomized value to an index
		}
		
		System.out.println("Array 1 holds the following integers: " + Arrays.toString(myIntArray)); // Print results
		
		// Loop through first array and count the numbers and their occurences
		for (int a = 0; a < myIntArray.length; a++) {
			numberOfOccurences = myIntArray[a];
			myIntOccurencesArray[numberOfOccurences]++;
		}
		// If occurences of numbers are more than two, print this condition, otherwise, print the other condition
		for (int b = 0; b < myIntOccurencesArray.length; b++) {
			if (myIntOccurencesArray[b] < 2) {
				System.out.println(b + " occurs " +  myIntOccurencesArray[b] + " time");
			} else {
				System.out.println(b + " occurs " +  myIntOccurencesArray[b] + " times");
			}
		}
	}
}