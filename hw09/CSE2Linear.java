// Sean Hong, 11/27/18, hw09 for CSE 002
// The purpose of this program is to compare method of searches as applied to arrays

import java.util.*; // Import all the classes

public class CSE2Linear {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create scanner instance
		int[] finalGrades = new int[15]; // Create array for final grades
		int currentGradeNumber = 0; // Create variable for input grade
		int gradeToBeFound = 0; // Create variable for finding desired grade
		
		System.out.println("Enter 15 ascending ints for final grades in CSE2: "); // Ask for grades
		for (int i = 0; i < finalGrades.length; i++) { // Allow for each element in array to be filled with a grade
			// Error check for valid ints and whether or not they meet certain conditions, if not, ask again
			while (myScanner.hasNext()) {
				if (myScanner.hasNextInt()) {
					currentGradeNumber = myScanner.nextInt();
					if (currentGradeNumber >= 0 && currentGradeNumber <= 100) {
						if (i <= 0) { // 0th index is automatically filled
							finalGrades[i] = currentGradeNumber;
							myScanner.nextLine();
							break;
						} else { // Compare next element to previous
							if (currentGradeNumber >= finalGrades[i - 1]) {
								finalGrades[i] = currentGradeNumber;
								myScanner.nextLine();
								break;
							} else { // Check for ascension
								System.out.print("Please enter another int greater than or equal to the last int: ");
								myScanner.nextLine();
							}
						}
					} else { // Check for ints between range
						System.out.print("Please enter an int between 0 - 100: ");
						myScanner.nextLine();
					}
				} else { // Check for valid int
					System.out.print("Please enter an int: ");
					myScanner.nextLine();
				}
			}
		}
		System.out.println(Arrays.toString(finalGrades)); // Print final array grades
		System.out.print("Enter a grade to search for: "); // Ask for desired grade to be found
		// Check for valid int
		while (myScanner.hasNext()) {
			if (myScanner.hasNextInt()) {
				gradeToBeFound = myScanner.nextInt();
				break;
			} else {
				System.out.print("Please enter an int: ");
				myScanner.nextLine();
			}
		}
		binarySearch(finalGrades, gradeToBeFound); // Binary search for the grade
		System.out.println("Scrambled: " + Arrays.toString(scrambleArray(finalGrades))); // Scramble array
		System.out.print("Enter a grade to search for: "); // Ask again for desired grade to be found
		// Check for valid int
		while (myScanner.hasNext()) {
			if (myScanner.hasNextInt()) {
				gradeToBeFound = myScanner.nextInt();
				break;
			} else {
				System.out.print("Please enter an int: ");
				myScanner.nextLine();
			}
		}
		linearSearch(finalGrades, gradeToBeFound); // Linear search for the grade
	}
	// Code adapted from Professor Carr's demonstration
	// Search array linearly until element is found, then it is returned, else return not found
	public static void linearSearch(int[] list, int key) {
		for (int i = 0; i < list.length; i++) {
			if (key == list[i]) {
				System.out.println(key + " was found after " + i + " iterations");
				break;
			} else {
				System.out.println(key + " was not found after " + i + " iterations");
			}
		}
	}
	// Code adapted from Professor Carr's demonstration
	// Search array by "dividing and conquering" until element is found, then it is returned, else return not found
	public static void binarySearch(int[] list, int key) {
		int steps = 0; // Counter for number of iterations
		int low = 0; // First half of the array
		int high = list.length - 1; // Second half of the array
		while (high >= low) {
			int mid = (low + high) / 2;
			if (key < list[mid]) {
				System.out.println(key + " was not found after " + steps + " iterations");
				high = mid - 1; // Allow for another "half" of the array
				steps++; // Add to step counter
			} else if (key == list[mid]) {
				System.out.println(key + " was found after " + steps + " iterations");
				break;
			} else {
				System.out.println(key + " was not found after " + steps + " iterations");
				low = mid + 1; // Allow for another "half" of the array
				steps++; // Add to step counter
			}
		}
	}
	// Scramble an array
	public static int[] scrambleArray(int[] list) {
		Random randGen = new Random();
		for (int i = 0; i < list.length; i++) {
			int randNum = randGen.nextInt(list.length - i); // Generate random index
			int temporaryHolder = list[i]; // Temporarily hold the list
			list[i] = list[i + randNum]; // Set subsequent indexes to a random index key
			list[i + randNum] = temporaryHolder; // Update the list
		}
		return list;
	}
}