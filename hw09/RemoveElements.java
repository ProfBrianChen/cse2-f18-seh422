// Sean Hong, 11/27/18, hw09 for CSE 002
// The purpose of this program is to manipulate elements within an array

import java.util.*; // Import all the classes

public class RemoveElements {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create scanner instance
		
		int num[] = new int[10]; // Create array for manipulation
		// Create new arrays after manipulation
		int newArray1[];
		int newArray2[];
		int index = 0; // Create variable for certain index to be manipulated
		int target = 0; // Create variable for certain element to be manipulated
		String answer = ""; 
		
		// Start it off then ask for it to be done again
		do {
			// Oringinal array is printed
			System.out.println("Random input of 10 ints [0-9]");
			num = randomInput();
			String out = "The original array is ";
			out += listArray(num);
			System.out.println(out);
			
			// Utilize delete method after asking for certain index to be deleted
			System.out.print("Enter the index: ");
			// Check to make sure index is valid
			while (myScanner.hasNext()) {
				if (myScanner.hasNextInt()) {
					index = myScanner.nextInt();
					if (index < 0 || index >= num.length) {
						System.out.print("The index does not exist. Enter another index: ");
						myScanner.nextLine();
					} else {
						System.out.println("Index " + index + " is removed");
						break;
					}
				}
			}
			newArray1 = delete(num, index);
			String out1 = "The output array is ";
			out1 += listArray(newArray1); // Return a string of the form "{2, 3, -9}"  
			System.out.println(out1);
			
			// Utilize remove method after asking for certain value to be removed
			System.out.print("Enter the target value: ");
			target = myScanner.nextInt();
			newArray2 = remove(num, target);
			String out2 = "The output array is ";
			out2 += listArray(newArray2); // Return a string of the form "{2, 3, -9}"  
			System.out.println(out2);
			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit: ");
			answer = myScanner.next();
		} while (answer.equals("Y") || answer.equals("y"));
	}
	// List array in pseudo-array form
	 public static String listArray(int num[]) {
		 String out = "{";
		 for(int j = 0; j < num.length; j++){
			 if (j > 0) {
				 out += ", ";
			 }
			 out += num[j];
		 }
		 out += "}";
		 return out;
	 }
	// Print an array with random value in each index
	public static int[] randomInput() {
		Random randGen = new Random();
		int[] newArray = new int[10];
		for (int i = 0; i < newArray.length; i++) {
			newArray[i] = randGen.nextInt(10);
		}
		return newArray;
	}
	// Delete index and its element from array
	public static int[] delete(int[] list, int pos) {
		int[] newArray = new int[list.length - 1]; // Create a new array after deleting index
		int counter = 0; // Counter for each index of new array
		for (int i = 0; i < list.length; i++) {
			if (i == pos) {
				continue;
			}
			newArray[counter++] = list[i];
		}
		return newArray;
	}
	// Remove duplicate values or value from array
	public static int[] remove(int[] list, int target) {
		int counter = 0; // Create a variable for counting number of duplicate values
		// Loop through array and add to counter
		for (int i = 0; i < list.length; i++) {
			if (list[i] == target) {
				counter++;
			}
		}
		// Check if element was found
		if (counter == 0) {
			System.out.println("Element " + target + " was not found");
			return list;
		} else {
			System.out.println("Element " + target + " was found");
		}
		int[] newArray = new int[list.length - counter]; // Create a new array without duplicate values
		int counterTwo = 0; // Counter for each element in new array
		// Loop through array and add to new array non duplicate values
		for (int i = 0; i < list.length; i++) {
			if (list[i] != target) {
				newArray[counterTwo] = list[i];
				counterTwo++;
			}
		}
		return newArray;
	}
}