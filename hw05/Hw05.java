// Sean Hong, 10/10/18, Hw 05 for CSE 002
// The purpose of this program is to calculate the probabilies of certain hands in poker
//

import java.util.*; // Import all the classes

public class Hw05 {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create the scanner instance
		
		// Create variables for how many hands to generate as well as whether or not to start the loop
		int numberOfGenerations = 0;
		boolean startLoop = true;
		
		// Counter for certain hands
		double numberOfFullHouse = 0;
		double numberOfFourKind = 0;
		double numberOfThreeKind = 0;
		double numberOfTwoPair = 0;
		double numberOfOnePair = 0;
		
		System.out.print("How many hands would you like generated: "); // Ask for how many hands to generate and begin looping
		while (startLoop) {
			if (myScanner.hasNextInt()) {
				numberOfGenerations = myScanner.nextInt();
				for (int counter = 1; counter <= numberOfGenerations; counter++) {
					
					// Create variables for each card
					int cardOne = (int) ((Math.random() * (52)) + 1);
					int cardTwo = (int) ((Math.random() * (52)) + 1);
					int cardThree = (int) ((Math.random() * (52)) + 1);
					int cardFour = (int) ((Math.random() * (52)) + 1);
					int cardFive = (int) ((Math.random() * (52)) + 1);
					
					// Prevent same numbers for card generation
					while (cardOne == cardTwo) {
						cardOne = (int) ((Math.random() * (52)) + 1);
					}
					
					while (cardTwo == cardOne || cardTwo == cardThree) {
						cardTwo = (int) ((Math.random() * (52)) + 1);
					}
					
					while (cardThree == cardOne || cardThree == cardTwo || cardThree == cardFour) {
						cardThree = (int) ((Math.random() * (52)) + 1);
					}
					
					while (cardFour == cardOne || cardFour == cardTwo || cardFour == cardThree || cardFour == cardFive) {
						cardFour = (int) ((Math.random() * (52)) + 1);
					}
					
					// Determine the number of the card
					int cardOneNumber = cardOne % 13;
					int cardTwoNumber = cardTwo % 13;
					int cardThreeNumber = cardThree % 13;
					int cardFourNumber = cardFour % 13;
					int cardFiveNumber = cardFive % 13;
					
					// Add one to the one pair counter
					if (cardOneNumber == cardTwoNumber || cardOneNumber == cardThreeNumber || cardOneNumber == cardFourNumber || cardOneNumber == cardFiveNumber ||
						  cardTwoNumber == cardThreeNumber || cardTwoNumber == cardFourNumber || cardTwoNumber == cardFiveNumber ||
							cardThreeNumber == cardFourNumber || cardThreeNumber == cardFiveNumber || cardFiveNumber == cardFourNumber
						 ) {
						numberOfOnePair++;
					}
					
					// Add one to the two pair counter
					if (cardOneNumber == cardTwoNumber && cardThreeNumber == cardFourNumber ||
						  cardOneNumber == cardTwoNumber && cardFourNumber == cardFiveNumber ||
							cardOneNumber == cardTwoNumber && cardThreeNumber == cardFiveNumber ||
							cardTwoNumber == cardThreeNumber && cardFourNumber == cardFiveNumber ||
							cardTwoNumber == cardThreeNumber && cardOneNumber == cardFourNumber ||
							cardTwoNumber == cardThreeNumber && cardOneNumber == cardFiveNumber ||
							cardThreeNumber == cardFourNumber && cardOneNumber == cardFiveNumber ||
							cardThreeNumber == cardFourNumber && cardTwoNumber == cardFiveNumber ||
							cardThreeNumber == cardFourNumber && cardOneNumber == cardTwoNumber ||
							cardFourNumber == cardFiveNumber && cardOneNumber == cardTwoNumber ||
							cardFourNumber == cardFiveNumber && cardOneNumber == cardThreeNumber ||
							cardFourNumber == cardFiveNumber && cardTwoNumber == cardThreeNumber
						 ) {
						numberOfTwoPair++;
					}
					
					// Add one to the three kind counter
					if (cardOneNumber == cardTwoNumber && cardTwoNumber == cardThreeNumber ||
						  cardOneNumber == cardThreeNumber && cardThreeNumber == cardFourNumber ||
							cardOneNumber == cardFourNumber && cardFourNumber == cardFiveNumber ||
							cardOneNumber == cardTwoNumber && cardTwoNumber == cardFiveNumber ||
							cardOneNumber == cardThreeNumber && cardThreeNumber == cardFiveNumber ||
							cardTwoNumber == cardThreeNumber && cardThreeNumber == cardFourNumber ||
							cardTwoNumber == cardFourNumber && cardFourNumber == cardFiveNumber ||
							cardThreeNumber == cardFourNumber && cardFourNumber == cardFiveNumber
						 ) {
						numberOfThreeKind++;
					}
					
					// Add one to the four kind counter
					if (cardOneNumber == cardTwoNumber && cardTwoNumber == cardThreeNumber && cardThreeNumber == cardFourNumber ||
						  cardOneNumber == cardThreeNumber && cardThreeNumber == cardFourNumber && cardFourNumber == cardFiveNumber ||
							cardTwoNumber == cardThreeNumber && cardThreeNumber == cardFourNumber && cardFourNumber == cardFiveNumber
						 ) {
						numberOfFourKind++;
					}
					
					// Stop loop if the number of generations have been met
					if (counter == numberOfGenerations) {
						startLoop = false;
					}
				}
			} else {
				System.out.print("Enter a valid number: "); // Loop won't start unless the number of generations is a valid number
				myScanner.next();
			}
		}
		// Print probabilities into terminal
		System.out.println("The number of loops: " + numberOfGenerations);
		System.out.printf("The probability of four-of-a-kind: %.3f %n", (numberOfFourKind / (double) numberOfGenerations));
		System.out.printf("The probability of three-of-a-kind: %.3f %n", (numberOfThreeKind / (double) numberOfGenerations));
		System.out.printf("The probability of two-pair: %.3f %n", (numberOfTwoPair / (double) numberOfGenerations));
		System.out.printf("The probability of one-pair: %.3f %n", (numberOfOnePair / (double) numberOfGenerations));
	}
}