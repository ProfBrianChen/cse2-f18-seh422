// Sean Hong, 12/6/18, lab10 for CSE 002
// The purpose of this program is to see the effects of insertion sort on different cases of arrays

import java.util.*; // Import all the classes

public class InsertionSortLab10 {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; // Best case for insertion sort
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1}; // Worst case for insertion sort
		// Sort
		int iterBest = insertionSort(myArrayBest);
		int iterWorst = insertionSort(myArrayWorst);
		// Print results
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
	public static int insertionSort(int[] list) {
		System.out.println(Arrays.toString(list));
		int iterations = 0;  // Create step counter
		int temp; // Create temp holder
		for (int i = 1; i < list.length; i++) {
			iterations++; // Update step counter
			for (int j = i; j > 0; j--) {
				if (list[j] < list[j - 1]) {
					// Begin insertion
					temp = list[j];
					list[j] = list[j - 1];
					list[j - 1] = temp;
					iterations++;
				}
				else {
					break; // End if sorted
				}
			}
			System.out.println(Arrays.toString(list));
		}
		return iterations;
	}
}