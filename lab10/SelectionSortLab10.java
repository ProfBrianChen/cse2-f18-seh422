// Sean Hong, 12/6/18, lab10 for CSE 002
// The purpose of this program is to see the effects of selection sort on different cases of arrays

import java.util.*; // Import all the classes

public class SelectionSortLab10 {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; // Best case for selection sort
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1}; // Worst case for selection sort
		// Sort
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		// Print results
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
	public static int selectionSort(int[] list) {
		System.out.println(Arrays.toString(list));
		int iterations = 0; // Create step counter
		for (int i = 0; i < list.length - 1; i++) {
			iterations++; // Update step counter
			int currentMin = list[i]; // Set current min
			int currentMinIndex = i; // Set current min index
			for (int j = i + 1; j < list.length; j++) {
				iterations++; // Update step counter
				if (list[j] < list[currentMinIndex]) {
					currentMinIndex = j; // Change current min index
				}
			}
			if (currentMinIndex != i) {
				// Begin swap
				int temp = list[currentMinIndex];
				list[currentMinIndex] = list[i];
				list[i] = temp;
				System.out.println(Arrays.toString(list));
			}
		}
		return iterations;
	}
}