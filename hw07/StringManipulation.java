// Sean Hong, 10/30/18, hw07 for CSE 002
// The purpose of this program is to utilize methods to dissect and analyze aspects of a certain string
//

import java.util.*; // Import all the classes

public class StringManipulation {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create a scanner instance
		
		String mainText = sampleText(); // Declare the text to be analyzed
		String desiredWord = null; // Set a variable to be used for case 'f'
		
		// Print user's text input to the terminal
		System.out.println();
		System.out.println("You entered: " + mainText);
		
		// Activiate certain methods from certain cases
		switch (printMenu()) {
			case "c": // Display case 'c' method
				System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(mainText));
				break;
			case "w": // Display case 'w' method
				System.out.println("Number of words: " + getNumOfWords(mainText));
				break;
			case "f": // Display case 'f' method
				System.out.print("Enter a word or phrase to be found: ");
				// Identify desired word to be found
				if (myScanner.hasNext()) {
					desiredWord = myScanner.next();
				}
				System.out.print("\"" + desiredWord + "\"" + " instances: " + findText(mainText, desiredWord));
				System.out.println();
				break;
			case "r": // Display case 'r' method
				System.out.println("Edited text: " + replaceExclamation(mainText));
				break;
			case "s": // Display case 's' method
				System.out.print("Edited text: " + shortenSpace(mainText));
				System.out.println();
				break;
			case "q": // Terminate code
				System.exit(0);
				break;
			default:
				break;
		}
	}
	public static String sampleText() {
		Scanner myScanner = new Scanner(System.in); // Create a scanner instance
		
		String enteredText = null; // Create a variable for user inputted text
		
		System.out.println("Enter a sample text:");
			while (enteredText == null) {
				if (myScanner.hasNext()) {
					enteredText = myScanner.nextLine(); // Get text
				}
			}
		return enteredText; // Return text
	}
	public static String printMenu() {
		Scanner myScanner = new Scanner(System.in); // Create a scanner instance
		
		String[] viableOptions = {"c", "w", "f", "r", "s", "q"}; // Create array for all the options
		String optionEntered = null; // Create a variable for the option entered
		
		// Display menu layout
		System.out.println();
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println();
		System.out.print("Choose an option: ");
		
		// Check if user input was a valid option
		while (true) {
			while (myScanner.hasNext()) {
				optionEntered = myScanner.next();
				if (Arrays.asList(viableOptions).contains(optionEntered)) {
					break;
				} else {
					System.out.println();
					System.out.println("invalid option");
					System.out.println();
					System.out.println("MENU");
					System.out.println("c - Number of non-whitespace characters");
					System.out.println("w - Number of words");
					System.out.println("f - Find text");
					System.out.println("r - Replace all !'s");
					System.out.println("s - Shorten spaces");
					System.out.println("q - Quit");
					System.out.println();
					System.out.print("Choose an option: ");
					myScanner.nextLine();
				}
			}
			break;
		}
		return optionEntered; // Return option entered
	}
	public static int getNumOfNonWSCharacters(String str) {
		int numberOfNonWSCharacters = 0;
		// Loop through the length of the string and count non white space characters
		for (int i = 0, v = str.length(); i < v; i++) {
			if (str.charAt(i) != ' ') {
				numberOfNonWSCharacters++;
			}
		}
		return numberOfNonWSCharacters; // Return value
	}
	public static int getNumOfWords(String words) {
		return words.split("\\w+").length; // Return number of words
	}
	public static int findText(String actualText, String keyWord) {
		int numberOfFoundText = 0;
		// Loop through text to count the desired word in it
		while (actualText.contains(keyWord)) {
			numberOfFoundText++;
			actualText = actualText.substring(actualText.indexOf(keyWord) + keyWord.length());
		}
		return numberOfFoundText; // Return value
	}
	public static String replaceExclamation(String textToBeReplaced) {
		return textToBeReplaced.replace("!", "."); // Return new text that replaces all exclamation points with a period
	}
	public static String shortenSpace(String bigText) {
		return bigText.trim().replaceAll(" +", " "); // Return new text with single spaces
	}
}