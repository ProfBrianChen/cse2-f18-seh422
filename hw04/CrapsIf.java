// Sean Hong, 9/24/18, hw04 for CSE 002
// The purpose of this program is to correspond generated dice values to its moniker in Craps
//
import java.util.Random; // Import the random class
import java.util.Scanner; // Import the scanner

public class CrapsIf {
  public static void main(String[] args) {
    Random randGen = new Random(); // Generate random numbers
    Scanner myScanner = new Scanner(System.in); // Create an instance called myScanner
    
    // Generate random dice values
    int rollOfDiceOne = randGen.nextInt(6) + 1;
    int rollOfDiceTwo = randGen.nextInt(6) + 1;
    
    // Declare a variable for user decision and two variables for choosing dice values
    boolean agreeToChoose;
    int chosenValueOfDiceOne;
    int chosenValueOfDiceTwo;
    
    // Prompt user for random dice casting or decisive dice casting
    System.out.print("Would you like to randomly cast the dice or choose their values? Type 'false' to randomly cast the dice or 'true' to input the dice values: ");
    agreeToChoose = myScanner.nextBoolean();
    
    // If the user decides false, the program will use the randomly generated dice values and connect it with its moniker in Craps
    if (agreeToChoose == false) {
      if (rollOfDiceOne + rollOfDiceTwo == 2) {
        System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as snake eyes");
      } else if (rollOfDiceOne + rollOfDiceTwo == 3) {
        System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as ace deuce");
      } else if (rollOfDiceOne + rollOfDiceTwo == 4) {
        if (rollOfDiceOne == 3 && rollOfDiceTwo == 1 || rollOfDiceOne == 1 && rollOfDiceTwo == 3) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as easy four");
        } else if (rollOfDiceOne == 2 && rollOfDiceTwo == 2) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as hard four");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 5) {
        if (rollOfDiceOne == 4 && rollOfDiceTwo == 1 || rollOfDiceOne == 1 && rollOfDiceTwo == 4 || rollOfDiceOne == 3 && rollOfDiceTwo == 2 || rollOfDiceOne == 2 && rollOfDiceTwo == 3) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as fever five");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 6) {
        if (rollOfDiceOne == 5 && rollOfDiceTwo == 1 || rollOfDiceOne == 1 && rollOfDiceTwo == 5 || rollOfDiceOne == 4 && rollOfDiceTwo == 2 || rollOfDiceOne == 2 && rollOfDiceTwo == 4) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as easy six");
        } else if (rollOfDiceOne == 3 && rollOfDiceTwo == 3) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as hard six");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 7) {
        if (rollOfDiceOne == 6 && rollOfDiceTwo == 1 || rollOfDiceOne == 1 && rollOfDiceTwo == 6 || rollOfDiceOne == 5 && rollOfDiceTwo == 2 || rollOfDiceOne == 2 && rollOfDiceTwo == 5 || rollOfDiceOne == 4 && rollOfDiceTwo == 3 || rollOfDiceOne == 3 && rollOfDiceTwo == 4) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as seven out");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 8) {
        if (rollOfDiceOne == 6 && rollOfDiceTwo == 2 || rollOfDiceOne == 2 && rollOfDiceTwo == 6 || rollOfDiceOne == 5 && rollOfDiceTwo == 3 || rollOfDiceOne == 3 && rollOfDiceTwo == 5) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as easy eight");
        } else if (rollOfDiceOne == 4 && rollOfDiceTwo == 4) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as hard eight");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 9) {
        if (rollOfDiceOne == 6 && rollOfDiceTwo == 3 || rollOfDiceOne == 3 && rollOfDiceTwo == 6 || rollOfDiceOne == 5 && rollOfDiceTwo == 4 || rollOfDiceOne == 4 && rollOfDiceTwo == 5) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as nine");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 10) {
        if (rollOfDiceOne == 6 && rollOfDiceTwo == 4 || rollOfDiceOne == 4 && rollOfDiceTwo == 6) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as easy ten");
        } else if (rollOfDiceOne == 5 && rollOfDiceTwo == 5) {
          System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as hard ten");
        }
      } else if (rollOfDiceOne + rollOfDiceTwo == 11) {
        System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as yo-leven");
      } else if (rollOfDiceOne + rollOfDiceTwo == 12) {
        System.out.println("The computer rolled a " + rollOfDiceOne + " for dice one and a " + rollOfDiceTwo + " for dice two, which resulted as boxcars");
      }
      // If the user decides true, the program will use the user-inputed dice values and connect it with its moniker in Craps
    } else if (agreeToChoose == true) {
      System.out.print("Input a value for dice one: ");
      chosenValueOfDiceOne = myScanner.nextInt();
      System.out.print("Input a value for dice two: ");
      chosenValueOfDiceTwo = myScanner.nextInt();
      
      // User-inputed dice values must be less than or equal to twelve to work
      if (chosenValueOfDiceOne + chosenValueOfDiceTwo <= 12 && chosenValueOfDiceOne + chosenValueOfDiceTwo != 0) {
        if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 2) {
          System.out.println("With the values you've chosen, you've resulted with snake eyes");
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 3) {
          System.out.println("With the values you've chosen, you've resulted with ace deuce");
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 4) {
          if (chosenValueOfDiceOne == 3 && chosenValueOfDiceTwo == 1 || chosenValueOfDiceOne == 1 && chosenValueOfDiceTwo == 3) {
            System.out.println("With the values you've chosen, you've resulted with easy four");
          } else if (chosenValueOfDiceOne == 2 && chosenValueOfDiceTwo == 2) {
            System.out.println("With the values you've chosen, you've resulted with hard four");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 5) {
          if (chosenValueOfDiceOne == 4 && chosenValueOfDiceTwo == 1 || chosenValueOfDiceOne == 1 && chosenValueOfDiceTwo == 4 || chosenValueOfDiceOne == 3 && chosenValueOfDiceTwo == 2 || chosenValueOfDiceOne == 2 && chosenValueOfDiceTwo == 3) {
            System.out.println("With the values you've chosen, you've resulted with fever five");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 6) {
          if (chosenValueOfDiceOne == 5 && chosenValueOfDiceTwo == 1 || chosenValueOfDiceOne == 1 && chosenValueOfDiceTwo == 5 || chosenValueOfDiceOne == 4 && chosenValueOfDiceTwo == 2 || chosenValueOfDiceOne == 2 && chosenValueOfDiceTwo == 4) {
            System.out.println("With the values you've chosen, you've resulted with easy six");
          } else if (chosenValueOfDiceOne == 3 && chosenValueOfDiceTwo == 3) {
            System.out.println("With the values you've chosen, you've resulted with hard six");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 7) {
          if (chosenValueOfDiceOne == 6 && chosenValueOfDiceTwo == 1 || chosenValueOfDiceOne == 1 && chosenValueOfDiceTwo == 6 || chosenValueOfDiceOne == 5 && chosenValueOfDiceTwo == 2 || chosenValueOfDiceOne == 2 && chosenValueOfDiceTwo == 5 || chosenValueOfDiceOne == 4 && chosenValueOfDiceTwo == 3 || chosenValueOfDiceOne == 3 && chosenValueOfDiceTwo == 4) {
            System.out.println("With the values you've chosen, you've resulted with seven out");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 8) {
          if (chosenValueOfDiceOne == 6 && chosenValueOfDiceTwo == 2 || chosenValueOfDiceOne == 2 && chosenValueOfDiceTwo == 6 || chosenValueOfDiceOne == 5 && chosenValueOfDiceTwo == 3 || chosenValueOfDiceOne == 3 && chosenValueOfDiceTwo == 5) {
            System.out.println("With the values you've chosen, you've resulted with easy eight");
          } else if (chosenValueOfDiceOne == 4 && chosenValueOfDiceTwo == 4) {
            System.out.println("With the values you've chosen, you've resulted with hard eight");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 9) {
          if (chosenValueOfDiceOne == 6 && chosenValueOfDiceTwo == 3 || chosenValueOfDiceOne == 3 && chosenValueOfDiceTwo == 6 || chosenValueOfDiceOne == 5 && chosenValueOfDiceTwo == 4 || chosenValueOfDiceOne == 4 && chosenValueOfDiceTwo == 5) {
            System.out.println("With the values you've chosen, you've resulted with nine");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 10) {
          if (chosenValueOfDiceOne == 6 && chosenValueOfDiceTwo == 4 || chosenValueOfDiceOne == 4 && chosenValueOfDiceTwo == 6) {
            System.out.println("With the values you've chosen, you've resulted with easy ten");
          } else if (chosenValueOfDiceOne == 5 && chosenValueOfDiceTwo == 5) {
            System.out.println("With the values you've chosen, you've resulted with hard ten");
          }
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 11) {
          System.out.println("With the values you've chosen, you've resulted with yo-leven");
        } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo == 12) {
          System.out.println("With the values you've chosen, you've resulted with boxcars");
        }
        // Any user-inputed dice values above twelve or equal to zero will result in a warning
      } else if (chosenValueOfDiceOne + chosenValueOfDiceTwo > 12 || chosenValueOfDiceOne + chosenValueOfDiceTwo == 0) {
        System.out.println("Please restart the program and choose values that range from 1-6 for each input value");
      }
    }
  }
}