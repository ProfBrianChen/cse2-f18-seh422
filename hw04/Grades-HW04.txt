Grading Rubric for HW04

Total: 77

Two Files were created CrapsIf.java & CrapsSwitch.java   10 pts yay
Random Code						                         10 pts yay
User provided input					                     2 pts -8
Determine Slang					                         5 pts -15
Print the Slang					                         10 pts yay
Comments						                         10 pts yay
Compiles						                         30 pts yay
***PROGRAM DOES NOT ACCOUNT FOR NUMBERS OUTSIDE OF RANGE 1-6
***PROGRAM DOES NOT ACCOUNT FOR INCORRECT ROLLING DICE INPUT
***PROGRAM DOES NOT APPROPRIATELY GENERATE/DECIDE RESULT