// Sean Hong, 9/24/18, hw04 for CSE 002
// The purpose of this program is to correspond generated dice values to its moniker in Craps
//
import java.util.Random; // Import the random class
import java.util.Scanner; // Import the scanner

public class CrapsSwitch {
  public static void main(String[] args) {
    Random randGen = new Random(); // Generate random numbers
    Scanner myScanner = new Scanner(System.in); // Create an instance called myScanner
    
    // Generate random dice values
    int rollOfDiceOne = randGen.nextInt(6) + 1;
    int rollOfDiceTwo = randGen.nextInt(6) + 1;
    
    // Declare a variable for user decision and two variables for choosing dice values
    int agreeToChoose;
    int chosenValueOfDiceOne;
    int chosenValueOfDiceTwo;
    
    // Prompt user for random dice casting or decisive dice casting
    System.out.print("Would you like to randomly cast the dice or choose their values? Type '0' to randomly cast the dice or '1' to input the dice values: ");
    agreeToChoose = myScanner.nextInt();
    
    // If the user decides false, the program will use the randomly generated dice values and connect it with its moniker in Craps
    switch (agreeToChoose) {
      case 0:
        switch (rollOfDiceOne + rollOfDiceTwo) {
          case 2: System.out.println("The computer resulted with snake eyes");
            break;
          case 3: System.out.println("The computer resulted with ace deuce");
            break;
          case 4: System.out.println("The computer resulted with either easy four or hard four");
            break;
          case 5: System.out.println("The computer resulted with fever five");
            break;
          case 6: System.out.println("The computer resulted with either easy six or hard six");
            break;
          case 7: System.out.println("The computer resulted with seven out");
            break;
          case 8: System.out.println("The computer resulted with either easy eight or hard eight");
            break;
          case 9: System.out.println("The computer resulted with nine");
            break;
          case 10: System.out.println("The computer resulted with either easy ten or hard ten");
            break;
          case 11: System.out.println("The computer resulted with yo-leven");
            break;
          case 12: System.out.println("The computer resulted with boxcars");
            break;
          default: System.out.println("The computer has produced an error in randomly generated dice numbers, please restart the program");
            break;
        }
        break;
      // If the user decides true, the program will use the user-inputed dice values and connect it with its moniker in Craps
      case 1:
        System.out.print("Input a value for dice one: ");
        chosenValueOfDiceOne = myScanner.nextInt();
        System.out.print("Input a value for dice two: ");
        chosenValueOfDiceTwo = myScanner.nextInt();
        switch (chosenValueOfDiceOne + chosenValueOfDiceTwo) {
          case 2: System.out.println("With the values you've chosen, you've resulted with snake eyes");
            break;
          case 3: System.out.println("With the values you've chosen, you've resulted with ace deuce");
            break;
          case 4: System.out.println("With the values you've chosen, you've resulted with either easy four or hard four");
            break;
          case 5: System.out.println("With the values you've chosen, you've resulted with fever five");
            break;
          case 6: System.out.println("With the values you've chosen, you've resulted with either easy six or hard six");
            break;
          case 7: System.out.println("With the values you've chosen, you've resulted with seven out");
            break;
          case 8: System.out.println("With the values you've chosen, you've resulted with either easy eight or hard eight");
            break;
          case 9: System.out.println("With the values you've chosen, you've resulted with nine");
            break;
          case 10: System.out.println("With the values you've chosen, you've resulted with either easy ten or hard ten");
            break;
          case 11: System.out.println("With the values you've chosen, you've resulted with yo-leven");
            break;
          case 12: System.out.println("With the values you've chosen, you've resulted with boxcars");
            break;
          default: System.out.println("Please restart the program and choose values that range from 1-6 for each input value");
            break;
        }
        break;
      default: System.out.println("Choice value was neither true or false, please restart the program");
        break;
    }
  }
}