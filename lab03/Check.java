// Sean Hong, 9/13/18, Lab 02 for CSE 002
// The purpose of this program is to determine how a group will split up a payment after eating dinner
//
import java.util.Scanner; // Import the scanner

public class Check {
    // Main method required for every Java program
    public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); // Create an instance called myScanner
      
      // Ask for the original cost of the check
      System.out.print("Enter the original cost of the check in the form xx.xx: ");
      double checkCost = myScanner.nextDouble();
      
      // Ask for the tip percentage
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
      double tipPercent = myScanner.nextDouble();
      tipPercent /= 100; // We want to convert the percentage into a decimal value
      
      // Ask for the amount of people who were in the group
      System.out.print("Enter the number of people who went out to dinner: ");
      int numPeople = myScanner.nextInt();
      
      double totalCost;
      double costPerPerson;
      int dollars, dimes, pennies; // Whole dollar amount of cost for storing digits to the right of the decimal point
      
      // Compute values
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople;
      dollars = (int) costPerPerson; // Get the whole amount
      dimes = (int) (costPerPerson * 10) % 10; // Get the amount of dimes from remainder
      pennies = (int) (costPerPerson * 100) % 10; // Get the amount of pennies from remainder
      
      // Print result into the terminal window
      System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
      
	}  // End of main method
} // End of class