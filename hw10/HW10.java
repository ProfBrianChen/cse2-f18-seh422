// Sean Hong, 12/4/18, hw10 for CSE 002
// The purpose of this program is to make a tic-tac-toe game functional

import java.util.*; // Import all the classes

public class HW10 {
  public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // Create scanner instance
		
		String[][] gameBoard = new String[3][3]; // Create the skeleton of the game board
		String currentPlayer = null; // Create a variable for the player
		int placementValue = 0; // Variable for the location on the game board to place mark
		String restartGame = null; // Variable to see whether or not player wants to play game again
		
		do {
			createBoard(gameBoard); // Create game
			////////////////////////////////////////////////////////////////////////////
			// Ask for player value
			System.out.print("Would you like to be 'x' or 'o'? -> ");
			while (myScanner.hasNext()) {
				currentPlayer = myScanner.next();
				if (currentPlayer.equals("x") || currentPlayer.equals("X")) { // Check for both cases of 'x'
					myScanner.nextLine();
					break;
				} else if (currentPlayer.equals("o") || currentPlayer.equals("O")) { // Check for both cases of 'o'
					myScanner.nextLine();
					break;
				} else {
					System.out.print("Please enter an 'x' or an 'o': "); // Validity check
					myScanner.nextLine();
				}
			}
			updateBoard(gameBoard); // Display game
			////////////////////////////////////////////////////////////////////////////
			// Play game
			while (!isWon(gameBoard)) { // Check if game is not won by anyone
				while (!isTied(gameBoard)) { // Check if game is not tied yet
					// Ask for location value for mark to be placed
					System.out.print("\nOkay player '" + currentPlayer + "', enter a number from the game board to place your mark! -> ");
					while (myScanner.hasNext()) {
						if (myScanner.hasNextInt()) {
							placementValue = myScanner.nextInt();
							if (placementValue > 0 && placementValue < 10) {
								if (!isSpotTaken(gameBoard, placementValue)) { // Check to see if spot is open
									placeMark(gameBoard, placementValue, currentPlayer); // Place the mark
									updateBoard(gameBoard); // Display board
									if (!isTied(gameBoard)) { // Check if game is still not tied
										if (!isWon(gameBoard)) { // Check if game is not won by anyone yet
											currentPlayer = switchPlayer(currentPlayer); // Change player
											myScanner.nextLine();
											break;
										} else {
											System.out.println("\nPlayer '" + currentPlayer + "' has won!"); // If game won, reveal winner 
											break;
										}
									} else {
										System.out.println("\nThe game ended in a draw!"); // If no one wins, reveal game is tied
										break;
									}
								} else {
									System.out.print("That spot has already been marked. Choose another location: "); // Check if spot is open
									myScanner.nextLine();
								}
							} else {
								System.out.print("Enter an integer between 1 and 9: "); // Validity check
								myScanner.nextLine();
							}
						} else {
							System.out.print("Enter an integer between 1 and 9: "); // Validity check
							myScanner.nextLine();
						}
					}
					if (isWon(gameBoard)) { // If game is won, end the 'not yet tied' loop
						break;
					}
				}
				if (isTied(gameBoard)) { // If game is tied, end the 'not yet won' loop
					break;
				}
			}
			////////////////////////////////////////////////////////////////////////////
			// Ask to restart game
			System.out.print("\nPlay again? Enter 'y' or 'Y', or anything else to quit: ");
			restartGame = myScanner.next();
		} while (restartGame.equals("Y") || restartGame.equals("y"));
	}
	// Create the board by placing a number in each spot
	public static void createBoard(String[][] board) {
		int counter = 0;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				counter++;
				board[i][j] = String.format("%d", counter); // Convert int to string
			}
		}
	}
	// Update and display the board
	public static void updateBoard(String[][] board) {
		System.out.println("\n-------------");
		for (int i = 0; i < board.length; i++) {
			System.out.print("| ");
			for (int j = 0; j < board[i].length; j++) {
				System.out.print(board[i][j] + " | ");
			}
			System.out.println();
			System.out.println("-------------");
		}
	}
	// Check every case for a win and return true if won or false if not
	public static boolean isWon(String[][] board) {
		if ((board[0][0]).equals(board[0][1]) && (board[0][0]).equals(board[0][2]) ||
				(board[1][0]).equals(board[1][1]) && (board[1][0]).equals(board[1][2]) ||
				(board[2][0]).equals(board[2][1]) && (board[2][0]).equals(board[2][2]) ||
				(board[0][0]).equals(board[1][0]) && (board[0][0]).equals(board[2][0]) ||
				(board[0][1]).equals(board[1][1]) && (board[0][1]).equals(board[2][1]) ||
				(board[0][2]).equals(board[1][2]) && (board[0][2]).equals(board[2][2]) ||
				(board[0][0]).equals(board[1][1]) && (board[0][0]).equals(board[2][2]) ||
				(board[0][2]).equals(board[1][1]) && (board[0][2]).equals(board[2][0]))
		{
			return true;
		} else {
			return false;
		}
	}
	// Change player after every move
	public static String switchPlayer(String player) {
		if (player.equals("x")) {
			return "o";
		} else if (player.equals("X")) {
			return "O";
		} else if (player.equals("o")) {
			return "x";
		} else if (player.equals("O")) {
			return "X";
		} else {
			return null;
		}
	}
	// Place mark at spot player chooses
	public static String[][] placeMark(String[][] board, int location, String playerMark) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if ((board[i][j]).equals(String.format("%d", location))) { // Check if location exists
					board[i][j] = playerMark;
				}
			}
		}
		return board;
	}
	// Check board for tie when all holder numbers have been replaced
	public static boolean isTied(String[][] board) {
		if (!(board[0][0]).equals("1") &&
				!(board[0][1]).equals("2") &&
				!(board[0][2]).equals("3") &&
				!(board[1][0]).equals("4") &&
				!(board[1][1]).equals("5") &&
				!(board[1][2]).equals("6") &&
				!(board[2][0]).equals("7") &&
				!(board[2][1]).equals("8") &&
				!(board[2][2]).equals("9") &&
				!isWon(board))
		{
			return true;
		} else {
			return false;
		}
	}
	// Check if spot on the game board is taken
	public static boolean isSpotTaken(String[][] board, int location) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if ((board[i][j]).equals(String.format("%d", location))) {
					return false;
				}
			}
		}
		return true;
	}
}