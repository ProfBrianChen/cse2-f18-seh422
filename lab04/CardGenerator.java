// Sean Hong, 9/20/18, Lab 04 for CSE 002
// The purpose of this program is to generate random cards from random numbers
//

import java.util.Random; // Import the random class

public class CardGenerator {
  public static void main(String[] args) {
    Random randGen = new Random(); // Generate random numbers
    
    // Generate random numbers for each 'value' of cards in each suit
    int numberOfCardForClubs = randGen.nextInt(13) + 1;
    int numberOfCardForHearts = randGen.nextInt(13) + 1;
    int numberOfCardForSpades = randGen.nextInt(13) + 1;
    int numberOfCardForDiamonds = randGen.nextInt(13) + 1;
    
    // Assign the value from randomly generated numbers to a card
    String nameOfCardForClubs;
    String nameOfCardForHearts;
    String nameOfCardForSpades;
    String nameOfCardForDiamonds;
    
    // Get the name of the card from the randomly generated number
    switch (numberOfCardForClubs) {
      case 1: nameOfCardForClubs = "Ace of Clubs";
              break;
      case 2: nameOfCardForClubs = "2 of Clubs";
              break;
      case 3: nameOfCardForClubs = "3 of Clubs";
              break;
      case 4: nameOfCardForClubs = "4 of Clubs";
              break;
      case 5: nameOfCardForClubs = "5 of Clubs";
              break;
      case 6: nameOfCardForClubs = "6 of Clubs";
              break;
      case 7: nameOfCardForClubs = "7 of Clubs";
              break;
      case 8: nameOfCardForClubs = "8 of Clubs";
              break;
      case 9: nameOfCardForClubs = "9 of Clubs";
              break;
      case 10: nameOfCardForClubs = "10 of Clubs";
              break;
      case 11: nameOfCardForClubs = "Jack of Clubs";
              break;
      case 12: nameOfCardForClubs = "Queen of Clubs";
              break;
      case 13: nameOfCardForClubs = "King of Clubs";
              break;
      default: nameOfCardForClubs = "Invalid Clubs Card";
              break;
    }
    // Get the name of card from the randomly generated number
    switch (numberOfCardForHearts) {
      case 1: nameOfCardForHearts = "Ace of Hearts";
              break;
      case 2: nameOfCardForHearts = "2 of Hearts";
              break;
      case 3: nameOfCardForHearts = "3 of Hearts";
              break;
      case 4: nameOfCardForHearts = "4 of Hearts";
              break;
      case 5: nameOfCardForHearts = "5 of Hearts";
              break;
      case 6: nameOfCardForHearts = "6 of Hearts";
              break;
      case 7: nameOfCardForHearts = "7 of Hearts";
              break;
      case 8: nameOfCardForHearts = "8 of Hearts";
              break;
      case 9: nameOfCardForHearts = "9 of Hearts";
              break;
      case 10: nameOfCardForHearts = "10 of Hearts";
              break;
      case 11: nameOfCardForHearts = "Jack of Hearts";
              break;
      case 12: nameOfCardForHearts = "Queen of Hearts";
              break;
      case 13: nameOfCardForHearts = "King of Hearts";
              break;
      default: nameOfCardForHearts = "Invalid Hearts Card";
              break;
    }
    // Get the name of the card from the randomly generated number
    switch (numberOfCardForSpades) {
      case 1: nameOfCardForSpades = "Ace of Spades";
              break;
      case 2: nameOfCardForSpades = "2 of Spades";
              break;
      case 3: nameOfCardForSpades = "3 of Spades";
              break;
      case 4: nameOfCardForSpades = "4 of Spades";
              break;
      case 5: nameOfCardForSpades = "5 of Spades";
              break;
      case 6: nameOfCardForSpades = "6 of Spades";
              break;
      case 7: nameOfCardForSpades = "7 of Spades";
              break;
      case 8: nameOfCardForSpades = "8 of Spades";
              break;
      case 9: nameOfCardForSpades = "9 of Spades";
              break;
      case 10: nameOfCardForSpades = "10 of Spades";
              break;
      case 11: nameOfCardForSpades = "Jack of Spades";
              break;
      case 12: nameOfCardForSpades = "Queen of Spades";
              break;
      case 13: nameOfCardForSpades = "King of Spades";
              break;
      default: nameOfCardForSpades = "Invalid Spades Card";
              break;
    }
    // Get the name of the card from the randomly generated number
    switch (numberOfCardForDiamonds) {
      case 1: nameOfCardForDiamonds = "Ace of Diamonds";
              break;
      case 2: nameOfCardForDiamonds = "2 of Diamonds";
              break;
      case 3: nameOfCardForDiamonds = "3 of Diamonds";
              break;
      case 4: nameOfCardForDiamonds = "4 of Diamonds";
              break;
      case 5: nameOfCardForDiamonds = "5 of Diamonds";
              break;
      case 6: nameOfCardForDiamonds = "6 of Diamonds";
              break;
      case 7: nameOfCardForDiamonds = "7 of Diamonds";
              break;
      case 8: nameOfCardForDiamonds = "8 of Diamonds";
              break;
      case 9: nameOfCardForDiamonds = "9 of Diamonds";
              break;
      case 10: nameOfCardForDiamonds = "10 of Diamonds";
              break;
      case 11: nameOfCardForDiamonds = "Jack of Diamonds";
              break;
      case 12: nameOfCardForDiamonds = "Queen of Diamonds";
              break;
      case 13: nameOfCardForDiamonds = "King of Diamonds";
              break;
      default: nameOfCardForDiamonds = "Invalid Diamonds Card";
              break;
    }
    // Print results into the terminal window
    System.out.println(nameOfCardForClubs);
    System.out.println(nameOfCardForHearts);
    System.out.println(nameOfCardForSpades);
    System.out.println(nameOfCardForDiamonds);
	}
}