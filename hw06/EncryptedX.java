// Sean Hong, 10/23/18, Hw 06 for CSE 002
// The purpose of this program is to display a hidden 'X' within a bunch of asterisks
//

import java.util.*; // Import all the classes

public class EncryptedX {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in); // Create the scanner instance

        // Establish variables for the input integer and whether or not the loop starts
        int inputValue = 0;
        boolean startLoop = true;

        // Establish variables for rows and column of output
        int numberOfRows = 0;
        int numberOfColumns = 0;

        while (startLoop) { // Start the loop
            System.out.print("Enter an integer between 0 and 100: "); // Ask for input
            // Check if input is valid
            while (myScanner.hasNext()) {
                if (myScanner.hasNextInt()) {
                    inputValue = myScanner.nextInt();
                    if (inputValue >= 0 && inputValue <= 100) {
                        break;
                    } else {
                        System.out.print("Please enter an integer between 0 and 100: ");
                        myScanner.nextLine();
                    }

                    System.out.print("Please enter a valid integer between 0 and 100: ");
                    myScanner.nextLine();
                }
            }
            if (inputValue % 2 == 0) { // Check if input is even
                // If even, create pattern
                for (numberOfRows = 0; numberOfRows < inputValue; numberOfRows++) {
                    for (numberOfColumns = 0; numberOfColumns < inputValue; ++numberOfColumns) {
                        if (numberOfRows == numberOfColumns || numberOfRows + numberOfColumns == inputValue - 1) {
                            System.out.print(" ");
                        } else {
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
            } else {
                // If odd, create pattern
                for (numberOfRows = 0; numberOfRows < inputValue; numberOfRows++) {
                    for (numberOfColumns = 0; numberOfColumns < inputValue; numberOfColumns++) {
                        if (numberOfRows == numberOfColumns || numberOfRows + numberOfColumns == inputValue - 1) {
                            System.out.print(" ");
                        } else {
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
            }
            startLoop = false; // End loop
        }
    }
}
