/// This code was created on 9/10/18 by Sean Hong for hw02 of CSE 002
/// The purpose of this program is to calculate the cost of bought items before and after sales tax
//
public class Arithmetic {
  public static void main(String args[]) {
    // Number of pairs of pants and the cost per pair of pants
    int numPants = 3;
    double pantsPrice = 34.98;
    
    // Number of sweatshirts and the cost per sweatshirt
    int numShirts = 2;
    double shirtPrice = 24.99;
    
    // Number of belts and the cost per belt
    int numBelts = 1;
    double beltCost = 33.99;
    
    // The tax rate in Pennsylvania
    double paSalesTax = 0.06;
    
    // Total cost of every individual item before sales tax
    double totalCostOfPants = (double) numPants * pantsPrice;
    double totalCostOfShirts = (double) numShirts * shirtPrice;
    double totalCostOfBelts = (double) numBelts * beltCost;
    
    // Total cost of every individual item after sales tax
    double totalCostOfPantsAfterTax = Math.round(((totalCostOfPants * (1.0 + paSalesTax)) * 100.0)) / 100.0;
    double totalCostOfShirtsAfterTax = Math.round(((totalCostOfShirts * (1.0 + paSalesTax)) * 100.0)) / 100.0;
    double totalCostOfBeltsAfterTax = Math.round(((totalCostOfBelts * (1.0 + paSalesTax)) * 100.0)) / 100.0;
    
    // Total cost of all items before and after sales tax
    double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    double totalCostAfterTax = totalCostOfPantsAfterTax + totalCostOfShirtsAfterTax + totalCostOfBeltsAfterTax;
    
    // Difference between items before and after tax
    double totalCostDifference = Math.round((totalCostAfterTax - totalCost) * 100.0) / 100.0;
    
    // Print results in the terminal window
    System.out.println("The current sales tax in Pennsylvania is " + (paSalesTax * 100.0) + "%");
    System.out.println("The total cost of pants before tax is $" + totalCostOfPants + ", whereas the total cost of sweatshirts before tax is $" + totalCostOfShirts + ", whereas the total cost of belts before tax is $" + totalCostOfBelts);
    System.out.println("The total cost of pants after tax is $" + totalCostOfPantsAfterTax + ", whereas the total cost of sweatshirts after tax is $" + totalCostOfShirtsAfterTax + ", whereas the total cost of belts after tax is $" + totalCostOfBeltsAfterTax);
    System.out.println("The total cost of each item before tax is $" + totalCost + ", whereas the total cost of each item after tax is $" + totalCostAfterTax);
    System.out.println("With the Pennsylvania sales tax, you have paid a total extra of $" + totalCostDifference);
  }
}