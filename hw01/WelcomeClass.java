//////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass {
  public static void main(String args[]) {
    /// Prints welcome message to terminal window
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-S--E--H--4--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    // Prints biographical statement
    System.out.println("Hi, my name is Sean Hong. I'm from Philadelphia and I like the Sixers and the Eagles.");
  }
}