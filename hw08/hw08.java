// Sean Hong, 11/15/18, hw08 for CSE 002
// The purpose of this program is to print a random hand of cards

import java.util.*; // Import all the classes

public class hw08 {
  public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); // Create the scanner instance
		
		// Create arrays representing a suit and rank
		String[] suitNames = {"C", "H", "S", "D"};
		String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
		
		// Create arrays for a deck and a hand
		String[] cards = new String[52];
		String[] hand = null;
		
		// Variables to be used in helper methods
		int numCards = 5;
		int again = 1;
		int index = 51;
		
		// Print out the initial deck
		for (int i = 0; i < cards.length; i++) {
			cards[i] = rankNames[i % 13] + suitNames[i / 13];
			System.out.print(cards[i] + " ");
		}
		
		System.out.println();
		printArray(cards); // Print initial deck again
		shuffle(cards); // Shuffle it
		printArray(cards); // Print shuffled deck
		
		// Continuously print hand out from shuffled deck
		while (again == 1) {
			if (index < numCards) {
				index = 51;
			}
			hand = getHand(cards, index, numCards);
			printArray(hand);
			index = index - numCards;
			System.out.println("Enter a 1 if you want another hand drawn");
			again = scan.nextInt();
		}
	}
	// Print array with a space separated each element
	public static void printArray(String[] list) {
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
	}
	// Shuffle elements in an array
	public static String[] shuffle(String[] list) {
		Random randGen = new Random();
		for (int i = 0; i < list.length; i++) {
			int randNum = randGen.nextInt(list.length - i); // Generate random index
			String temporaryHolder = list[i]; // Temporarily hold the list
			list[i] = list[i + randNum]; // Set subsequent indexes to a random index key
			list[i + randNum] = temporaryHolder; // Update the list
		}
		System.out.println("Shuffled");
		return list;
	}
	// Return hand from deck
	public static String[] getHand(String[] list, int index, int numCards) {
		// Create variables for old hand and new hand
		String[] oldHand = list;
		String[] newHand = null;
		
		if (index > numCards) { // Print elements from last index of array down
			newHand = new String[numCards];
			for (int i = 0; i < newHand.length; i++) {
				newHand[i] = list[index];
				index = index - 1;
			}
		} else { // Create new deck
			newHand = new String[52];
			newHand = oldHand;
			shuffle(newHand);
		}
		System.out.println("Hand");
		return newHand;
	}
}