// Sean Hong, 11/8/18, lab07 for CSE 002
// The purpose of this program is to generate a nonsensical paragraph
//

import java.util.*; // Import all the classes

public class GrammarTwo {
  public static void main(String[] args) {
		// Create instances for a scanner and a randomizer
		Scanner myScanner = new Scanner(System.in);
		Random randGen = new Random();
		
		// Generate a random number for how many sentences will be created after thesis sentence
		int randNum = randGen.nextInt(10) + 1;
		
		// Create variables for each part of a sentence
		String partOne = null;
		String partTwo = null;
		String partThree = null;
		String partFour = null;
		String partFive = null;
		String partSix = null;
		
		// Create variables from the prompt
		String mainThesis = null;
		String actionSentence = "";
		String conclusionSentence = null;
		
		// Set the sentence parts
		partOne = generateAdjective();
		partTwo = generateAdjective();
		partThree = generateSubjectNoun();
		partFour = generateVerb();
		partFive = generateAdjective();
		partSix = generateObjectNoun();
		
		// Prevent repeating adjectives
		while (partOne == partTwo) {
			partTwo = generateAdjective();
		}
		
		// Create the main thesis
		mainThesis = generateThesisStatement(partOne, partTwo, partThree, partFour, partFive, partSix);
		
		// Create the conclusion
		while (conclusionSentence == null) {
			conclusionSentence = generateConclusionSentence(partThree, generateVerb(), generateObjectNoun());
		}
		
		// Create the action sentences depending on the randomly generated number
		for (int i = 0; i <= randNum; i++) {
			actionSentence = actionSentence + generateActionSentence(partThree, generateObjectNoun(), generateVerb(), generateObjectNoun(), generateAdjective(), generateObjectNoun());
		}
		// Print it all out
		System.out.println(mainThesis + actionSentence + conclusionSentence);
	}
	// Generate a random adjective from a randomly generated number
	public static String generateAdjective() {
		Random randGenOne = new Random();
		int randNumOne = randGenOne.nextInt(10);
		String randAdjective = null;
		switch (randNumOne) {
			case 0: randAdjective = "hysterical";
				break;
			case 1: randAdjective = "possessive";
				break;
			case 2: randAdjective = "savory";
				break;
			case 3: randAdjective = "ugly";
				break;
			case 4: randAdjective = "fat";
				break;
			case 5: randAdjective = "instinctive";
				break;
			case 6: randAdjective = "grateful";
				break;
			case 7: randAdjective = "nostalgic";
				break;
			case 8: randAdjective = "daffy";
				break;
			case 9: randAdjective = "historical";
				break;
			default: randAdjective = "error";
				break;
		}
		return randAdjective;
	}
	// Generate a random subject noun from a randomly generated number
	public static String generateSubjectNoun() {
		Random randGenTwo = new Random();
		int randNumTwo = randGenTwo.nextInt(10);
		String randSubjectNoun = null;
		switch (randNumTwo) {
			case 0: randSubjectNoun = "book";
				break;
			case 1: randSubjectNoun = "steel";
				break;
			case 2: randSubjectNoun = "squirrel";
				break;
			case 3: randSubjectNoun = "tooth";
				break;
			case 4: randSubjectNoun = "passenger";
				break;
			case 5: randSubjectNoun = "zebra";
				break;
			case 6: randSubjectNoun = "insect";
				break;
			case 7: randSubjectNoun = "robin";
				break;
			case 8: randSubjectNoun = "boot";
				break;
			case 9: randSubjectNoun = "teacher";
				break;
			default: randSubjectNoun = "error";
				break;
		}
		return randSubjectNoun;
	}
	// Generate a random verb from a randomly generated number
	public static String generateVerb() {
		Random randGenThree = new Random();
		int randNumThree = randGenThree.nextInt(10);
		String randVerb = null;
		switch (randNumThree) {
			case 0: randVerb = "kissed";
				break;
			case 1: randVerb = "jumped";
				break;
			case 2: randVerb = "looked";
				break;
			case 3: randVerb = "discovered";
				break;
			case 4: randVerb = "opened";
				break;
			case 5: randVerb = "cleaned";
				break;
			case 6: randVerb = "pressed";
				break;
			case 7: randVerb = "dressed";
				break;
			case 8: randVerb = "warned";
				break;
			case 9: randVerb = "invented";
				break;
			default: randVerb = "error";
				break;
		}
		return randVerb;
	}
	// Generate a random object noun from a randomly generated number
	public static String generateObjectNoun() {
		Random randGenFour = new Random();
		int randNumFour = randGenFour.nextInt(10);
		String randObjectNoun = null;
		switch (randNumFour) {
			case 0: randObjectNoun = "point";
				break;
			case 1: randObjectNoun = "doll";
				break;
			case 2: randObjectNoun = "thought";
				break;
			case 3: randObjectNoun = "wrench";
				break;
			case 4: randObjectNoun = "parcel";
				break;
			case 5: randObjectNoun = "gate";
				break;
			case 6: randObjectNoun = "train";
				break;
			case 7: randObjectNoun = "discussion";
				break;
			case 8: randObjectNoun = "phone";
				break;
			case 9: randObjectNoun = "computer";
				break;
			default: randObjectNoun = "error";
				break;
		}
		return randObjectNoun;
	}
	// Generate a thesis statement from previously generated parts of a sentence in the main method
	public static String generateThesisStatement(String a, String b, String c, String d, String e, String f) {
		String mainThesis = "The " + a + " " + b + " " + c + " " + d + " the " + e + " " + f + ".";
		return mainThesis;
	}
	// Generate an action sentence with alternating scenarios for a subject's pronoun in main method
	public static String generateActionSentence(String a, String b, String c, String d, String e, String f) {
		Random randGenFive = new Random();
		int randNumFive = randGenFive.nextInt(2);
		String sentenceSubject = a;
		String actionSentence = null;
		switch (randNumFive) {
			case 0: actionSentence = " It used " + b + " to " + c.replace("ed", "") + " " + d + " at the " + e + " " + f + ".";
				break;
			case 1: actionSentence = " The " + a + " used " + b + " to " + c.replace("ed", "") + " " + d + " at the " + e + " " + f + ".";
				break;
			default: actionSentence = "error";
				break;
		}
		return actionSentence;
	}
	// Generate a conclusion statement from parts of a sentence in main method
	public static String generateConclusionSentence(String a, String b, String c) {
		return " In the end, that " + a + " " + b + " the " + c + ".";
	}
}