// Sean Hong, 11/8/18, lab07 for CSE 002
// The purpose of this program is to generate many nonsensical sentences
//

import java.util.*; // Import all the classes

public class Grammar {
  public static void main(String[] args) {
		// Create instances for a scanner and a randomizer
		Scanner myScanner = new Scanner(System.in);
		Random randGen = new Random();
		
		// Create variables for randomizing numbers and a user inputted decision
		int randNum = randGen.nextInt(99) + 1;
		int choiceNumber = 0;
		
		// Create variables for parts of a sentence
		String partOne = null;
		String partTwo = null;
		String partThree = null;
		String partFour = null;
		String partFive = null;
		String partSix = null;
		
		// Generate many nonsensical sentences
		for (int i = 0; i <= randNum; i++) {
			partOne = generateAdjective();
			partTwo = generateAdjective();
			partThree = generateSubjectNoun();
			partFour = generateVerb();
			partFive = generateAdjective();
			partSix = generateObjectNoun();
			while (partOne == partTwo) { // Prevent repeating adjectives
				partTwo = generateAdjective();
			}
			// Print it all out
			System.out.println("The " + partOne + " " + partTwo + " " + partThree + " " + partFour + " the " + partFive + " " + partSix + ".");
		}
		
		// Ask if user wants to generate more nonsensical sentences
		while (true) {
			System.out.print("Would you like to generate more sentences? Reply with '0' for NO or '1' for YES: ");
			while (myScanner.hasNext()) {
				if (myScanner.hasNextInt()) {
					choiceNumber = myScanner.nextInt();
					if (choiceNumber == 0) { // No? End loop then end program
						break;
					} else if (choiceNumber == 1) { // Yes? Generate more nonsensical sentences
						randNum = randGen.nextInt(99) + 1;
						for (int j = 0; j <= randNum; j++) {
							partOne = generateAdjective();
							partTwo = generateAdjective();
							partThree = generateSubjectNoun();
							partFour = generateVerb();
							partFive = generateAdjective();
							partSix = generateObjectNoun();
							
							while (partOne == partTwo) { // Prevent repeating adjectives
								partTwo = generateAdjective();
							}
							// Print it all out
							System.out.println("The " + partOne + " " + partTwo + " " + partThree + " " + partFour + " the " + partFive + " " + partSix + ".");
						}
						// Ask for choice
						System.out.print("Would you like to generate more sentences? Reply with '0' for NO or '1' for YES: ");
						myScanner.nextLine();
					} else {
						// Make sure choice is valid
						System.out.print("Please enter a valid choice: ");
						myScanner.nextLine();
					}
				} else {
					// Make sure choice is valid
					System.out.print("Please enter a valid choice: ");
					myScanner.nextLine();
				}
			}
			break;
		}
	}
	// Generate a random adjective from a randomly generated number
	public static String generateAdjective() {
		Random randGenOne = new Random();
		int randNumOne = randGenOne.nextInt(10);
		String randAdjective = null;
		switch (randNumOne) {
			case 0: randAdjective = "hysterical";
				break;
			case 1: randAdjective = "possessive";
				break;
			case 2: randAdjective = "savory";
				break;
			case 3: randAdjective = "ugly";
				break;
			case 4: randAdjective = "fat";
				break;
			case 5: randAdjective = "instinctive";
				break;
			case 6: randAdjective = "grateful";
				break;
			case 7: randAdjective = "nostalgic";
				break;
			case 8: randAdjective = "daffy";
				break;
			case 9: randAdjective = "historical";
				break;
			default: randAdjective = "error";
				break;
		}
		return randAdjective;
	}
	// Generate a random subject noun from a randomly generated number
	public static String generateSubjectNoun() {
		Random randGenTwo = new Random();
		int randNumTwo = randGenTwo.nextInt(10);
		String randSubjectNoun = null;
		switch (randNumTwo) {
			case 0: randSubjectNoun = "book";
				break;
			case 1: randSubjectNoun = "steel";
				break;
			case 2: randSubjectNoun = "squirrel";
				break;
			case 3: randSubjectNoun = "tooth";
				break;
			case 4: randSubjectNoun = "passenger";
				break;
			case 5: randSubjectNoun = "zebra";
				break;
			case 6: randSubjectNoun = "insect";
				break;
			case 7: randSubjectNoun = "robin";
				break;
			case 8: randSubjectNoun = "boot";
				break;
			case 9: randSubjectNoun = "teacher";
				break;
			default: randSubjectNoun = "error";
				break;
		}
		return randSubjectNoun;
	}
	// Generate a random verb from a randomly generated number
	public static String generateVerb() {
		Random randGenThree = new Random();
		int randNumThree = randGenThree.nextInt(10);
		String randVerb = null;
		switch (randNumThree) {
			case 0: randVerb = "ate";
				break;
			case 1: randVerb = "jumped";
				break;
			case 2: randVerb = "charged";
				break;
			case 3: randVerb = "discovered";
				break;
			case 4: randVerb = "poked";
				break;
			case 5: randVerb = "raised";
				break;
			case 6: randVerb = "pressed";
				break;
			case 7: randVerb = "dressed";
				break;
			case 8: randVerb = "warned";
				break;
			case 9: randVerb = "invented";
				break;
			default: randVerb = "error";
				break;
		}
		return randVerb;
	}
	// Generate a random object noun from a randomly generated number
	public static String generateObjectNoun() {
		Random randGenFour = new Random();
		int randNumFour = randGenFour.nextInt(10);
		String randObjectNoun = null;
		switch (randNumFour) {
			case 0: randObjectNoun = "point";
				break;
			case 1: randObjectNoun = "doll";
				break;
			case 2: randObjectNoun = "thought";
				break;
			case 3: randObjectNoun = "wrench";
				break;
			case 4: randObjectNoun = "parcel";
				break;
			case 5: randObjectNoun = "gate";
				break;
			case 6: randObjectNoun = "train";
				break;
			case 7: randObjectNoun = "discussion";
				break;
			case 8: randObjectNoun = "phone";
				break;
			case 9: randObjectNoun = "computer";
				break;
			default: randObjectNoun = "error";
				break;
		}
		return randObjectNoun;
	}
}